import 'package:country_app/blocs/bloc.dart';
import 'package:country_app/repository/city_repository.dart';
import 'package:country_app/ui/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final fromApi = FetchDataFromApi();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (BuildContext context) => CityBloc(fromApi: fromApi),
        child: MyHomePage(),
      ),
    );
  }
}
