import 'package:country_app/blocs/events.dart';
import 'package:country_app/blocs/states.dart';
import 'package:country_app/models/city_model.dart';
import 'package:country_app/repository/city_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CityBloc extends Bloc<CityEvent, CityState> {
  FetchDataFromApi fromApi;
  List<Destination> listOfCityList = [];
  List<Destination> _searchedList = [];

  CityBloc({@required this.fromApi}) : super(Loading(false));

  @override
  Stream<CityState> mapEventToState(CityEvent event) async* {
    if (event is FetchData) {
      yield Loading(true);
      try {
        listOfCityList = await fromApi.cityFromJsonDecoded();

        yield Loaded(listOfCityList: listOfCityList);
      } catch (e) {
        print('errorData: $e');
        yield Error();
      } finally {
        yield Loading(false);
      }
    }
    if (event is InputData) {
      _searchedList = fromApi.searchCity(event.input, listOfCityList);
      yield Searched(listOfCitySearched: _searchedList);
    }
    if (event is Choice) {
      yield HomePage(cityChosen: event.choice);
    }
  }
}
